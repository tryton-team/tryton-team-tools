# The default profile for debian.tryton.org.
# Used to avoid:
# bad-distribution-in-changes-file
# lintian errors on backport builds
Profile: debian_tryton_org/main
Extends: debian/main
Disable-Tags: newer-standards-version,
 latest-debian-changelog-entry-without-new-version,
 data.tar.xz-member-without-dpkg-pre-depends
